import numpy as np
from thi_oe import Faculty


class Member:
    def __init__(self, name, university):
        self.name = name
        self.university = university


class FacultyMember(Member):
    def __init__(self, faculty, **kwargs):
        super().__init__(**kwargs)
        self.faculty = faculty


class Staff(FacultyMember):
    def __init__(self, salary, research_area, **kwargs):
        super().__init__(**kwargs)
        self.salary = salary
        self.research_area = research_area


class Student(FacultyMember):
    def __init__(self, enrolment_key, grades, semester, **kwargs):
        super().__init__(**kwargs)
        self.enrolment_key = enrolment_key
        self.grades = grades or {'math': 2}
        self.semester = semester
        self.usual_study_duration = 7

    def compute_mean_grade(self):
        return np.mean(self.grades)

    def get_grade(self, subject='math'):
        return print(self.grades[subject])

    def estimate_graduation(self):
        semesters_to_go = self.usual_study_duration - self.semester
        return "Graduation is expected in " + semesters_to_go + "semesters."


if __name__ == '__main__':
    wi_faculty = Faculty(subject='WI', 
                         name='THI',
                         city='Ingolstadt')
    student = Student(enrolment_key='123456',
                      grades=[1, 2, 1, 3],
                      semester=2,
                      name='Peter',
                      university='THI',
                      faculty='WI')

    dean = Staff(salary='60k',
                 faculty=wi_faculty,
                 name='Bob',
                 university='THI',
                 research_area='BWL')
    dean.salary = 'tr'

    print(student.name)
    print(dean.salary)
