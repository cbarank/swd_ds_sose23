import numpy as np


class University:
    def __init__(self, city, name):
        self.name = name
        self.city = city


class Faculty(University):
    def __init__(self, subject, **kwargs):
        super().__init__(**kwargs)
        self.subject = subject


class Board(University):
    def __init__(self, function, **kwargs):
        super().__init__(**kwargs)
        self.function = function


class Building:
    def __init__(self, number_rooms):
        self.number_rooms = number_rooms


if __name__ == '__main__':
    pass
