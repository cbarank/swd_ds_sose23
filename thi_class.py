import numpy as np


class THIMember:
    def __init__(self, name, university):
        self.name = name
        self.university = university


class FacultyMember(THIMember):
    def __init__(self, faculty, name, university):
        super().__init__(name, university)
        self.faculty = faculty


class Staff(FacultyMember):
    def __init__(self, salary, research_area, faculty, name, university):
        super().__init__(faculty, name, university)
        self.salary = salary
        self.research_area = research_area


class Student(FacultyMember):
    def __init__(self, enrolment_key, grades, semester, **kwargs):
        super().__init__(**kwargs)
        self.enrolment_key = enrolment_key
        self.grades = grades
        self.semester = semester
        self.usual_study_duration = 7

    def compute_mean_grade(self):
        return np.mean(self.grades)

    def estimate_graduation(self):
        semesters_to_go = self.usual_study_duration - self.semester
        return "Graduation is expected in " + semesters_to_go + "semesters."


def adder(x, y):
    return x + y


class Line:
    def __init__(self, point_a, point_b):
        self.pt_a = point_a
        self.pt_b = point_b

    def compute_distance(self):
        distance = (
                       (self.pt_a[0] - self.pt_b[0])**2 +
                       (self.pt_a[1] - self.pt_b[1])**2
                   )**0.5
        return distance


if __name__ == '__main__':
    student = Student(enrolment_key='123456',
                      grades=[1, 2, 1, 3],
                      semester=2,
                      name='Peter',
                      university='THI',
                      faculty='WI')

    dean = Staff(salary='60k',
                 faculty='WI',
                 name='Bob',
                 university='THI',
                 research_area='BWL')
    dean.salary = 'tr'

    print(student.name)
    print(dean.salary)
