import unittest

import pandas
import pandas as pd

import matplotlib

from utils.data_utils import comma2dot
from src.data import DataReader
from src.gui import PlotterGUI
from src.plotter import Plotter


class TestPlot(unittest.TestCase):
    def setUp(self):
        self.plotter_gui = PlotterGUI()
        self.data_reader = DataReader(view=self.plotter_gui)
        self.data_reader.file_path = \
            '/Users/oelkerm/temp/swd_ds_sose23/plotter/tests/integration' \
            '/fixtures/aggroTV.csv'
        self.data_reader.selected_features = ['Zeitdauer', 'Aggressivität']
        self.data_reader.data = self.data_reader.read()

        self.plotter = Plotter(view=self.plotter_gui, reader=self.data_reader)

    def test_plot(self):
        self.plotter.testrun = True

        ax = self.plotter.scatter_plot()

        self.assertIsInstance(ax, matplotlib.axes._axes.Axes)


if __name__ == '__main__':
    unittest.main()
