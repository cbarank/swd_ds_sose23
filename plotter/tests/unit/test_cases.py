import unittest

import pandas
import pandas as pd

from utils.data_utils import comma2dot
from src.data import DataReader
from src.gui import PlotterGUI


class TestComma2dot(unittest.TestCase):
    def test_data_frame_w_commata(self):
        """
        Test that a dataframe containing strings with commata can be
        converted to a dataframe containing floats
        """
        data = pd.DataFrame({'Preis': ['5,6', '1,2', '4,7'], 'Menge': [5, 6, 7]})
        result = comma2dot(df=data, column='Preis')

        for _, row in result.iterrows():
            self.assertIsInstance(row['Preis'], float)

    def test_data_frame_w_other_data_type(self):
        """
        Test that a dataframe containing strings with commata can be
        converted to a dataframe containing floats
        """
        data = pd.DataFrame({'Preis': [5.6, '1,2', '4,7'], 'Menge': [5, 6, 7]})
        result = comma2dot(df=data, column='Preis')

        for _, row in result.iterrows():
            self.assertIsInstance(row['Preis'], float)


class TestDataReader(unittest.TestCase):
    def setUp(self):
        plotter_gui = PlotterGUI()
        self.data_reader = DataReader(view=plotter_gui)

    def test_file_loading_faulty_path(self):
        # Erstellen von Testdaten
        test_path = '/some/nonsense/path'
        self.data_reader.file_path = test_path

        # Funktion der SW genutzt
        read_data = self.data_reader.read()

        # Vergleich von ground truth und SW
        self.assertFalse(read_data)

    def test_file_loading_true_path(self):
        # Erstellen von Testdaten
        test_path = '/Users/oelkerm/Teaching/SoftwareDevelopment_DataScience' \
                    '/swd_ds_sose23/plotter/tests/fixtures/test.csv'
        self.data_reader.file_path = test_path

        # Funktion der SW genutzt
        read_data = self.data_reader.read()

        # Vergleich von ground truth und SW
        self.assertIsInstance(read_data, pandas.DataFrame)


class TestGUI(unittest.TestCase):
    pass


if __name__ == '__main__':
    unittest.main()
