# User Requirement Specification

1. Software (SW) soll .csv-files einlesen können. 
2. SW soll aus jeweils zwei frei wählbaren Spalten eines .csv files ein 
   Scatter Plot erzeugen können. 
3. User soll Farbe, Form, Aussehen und weitere relevante Charakteriska von 
   Scatter Plots frei wählen können. 
4. Die Interaktion von User und SW soll mittels einer GUI erfolgen, die per 
   command line interface (CLI) gestartet wird. 
5. Der User soll den erzeugten Plot speichern können. 
