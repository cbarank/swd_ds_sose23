from tkinter import filedialog


def open_file_dialog():
    return filedialog.askopenfilename()
