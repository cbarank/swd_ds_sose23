def comma2dot(df, column):
    for idx, elem in enumerate(df[column]):
        if not isinstance(elem, str):
            continue
        assert isinstance(elem, str), 'DataFrame element is not of type str'
        numeric_value = float(elem.replace(',', '.'))
        df.loc[idx, column] = numeric_value
    return df


def comma2dot_alt(df, column):
    return df[column].apply(lambda x: float(x.replace(',', '.')))
