from src.gui import PlotterGUI
from src.data import DataReader
from src.plotter import Plotter


def main():
    plotter_gui = PlotterGUI()
    data_reader = DataReader(view=plotter_gui)
    Plotter(reader=data_reader, view=plotter_gui)

    plotter_gui.show()


if __name__ == "__main__":
    # ToDo: file path as argument

    main()
