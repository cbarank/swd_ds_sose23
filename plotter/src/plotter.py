import matplotlib.pyplot as plt


class Plotter:
    def __init__(self, reader, view):
        self._reader = reader
        self._view = view
        self.features = self._reader.selected_features
        self._view.buttons['btn_plot'].configure(command=self.scatter_plot)
        self.testrun = False

    def scatter_plot(self):
        self._reader.data.plot.scatter(x=self.features[0], y=self.features[1])
        if not self.testrun:
            plt.show()
        if self.testrun:
            return self._reader.data.plot.scatter(
                x=self.features[0], y=self.features[1])
