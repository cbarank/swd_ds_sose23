import pandas as pd
import tkinter as tk

from utils.gui_utils import open_file_dialog


class DataReader:
    def __init__(self, view):
        self.file_path = ''
        self.selected_features = []
        self.data = None
        self._view = view
        self._view.buttons['btn_open'].configure(
            command=self.get_data_from_file)
        self._view.buttons['feat_list'].configure(command=self.list_features)
        self._view.buttons['get_data'].configure(
            command=self.get_data_from_selected_features)

    def read(self):
        try:
            with open(self.file_path, 'r') as input_file:
                return pd.read_csv(input_file)
        except FileNotFoundError as fnf_error:
            return False

    def list_features(self):
        features = self.data.columns.values.tolist()
        for idx, each_feature in enumerate(features):
            self._view.features['feature_listbox'].insert(tk.END, each_feature)
            self._view.features['feature_listbox'].itemconfig(idx, bg="lime")

    def get_data_from_selected_features(self):
        self.get_selected_features()
        self.data = self.data[self.selected_features]

    def get_selected_features(self):
        for selected_feat in self._view.features[
                'feature_listbox'].curselection():
            selected_feature = self._view.features[
                                         'feature_listbox'].get(selected_feat)
            self.selected_features.append(selected_feature)

    def get_data_from_file(self):
        self.file_path = open_file_dialog()
        self.data = self._read()
