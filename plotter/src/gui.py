import tkinter as tk


class PlotterGUI:
    def __init__(self):
        self.win = tk.Tk()
        self.generate_layout()
        self.buttons = self.generate_buttons()
        self.features = self.generate_feature_window()

    def show(self):
        self.win.mainloop()

    def generate_layout(self):
        self.win.title("Simple Plotter")

    def generate_buttons(self):
        frm_buttons = tk.Frame(self.win, relief=tk.RAISED, bd=2)
        btn_open = tk.Button(frm_buttons, text="Open")
        btn_open.grid(row=0, column=0, sticky="ns")
        btn_plot = tk.Button(frm_buttons, text="Plot")
        btn_plot.grid(row=3, column=0, sticky="ns")
        feat_list = tk.Button(frm_buttons, text="Feature Selection")
        feat_list.grid(row=1, column=0, sticky="ns")
        get_data = tk.Button(frm_buttons, text="Load Selected Features")
        get_data.grid(row=2, column=0, sticky="ns")
        frm_buttons.grid(row=0, column=0, sticky="ns")
        return {'btn_open': btn_open, 'btn_plot': btn_plot, 'feat_list':
                feat_list, 'get_data': get_data}

    def generate_feature_window(self):
        # ToDo: Make feature frame smaller, and add scrollbar
        frm_features = tk.Frame(self.win, relief=tk.RAISED, bd=2)
        # yscrollbar = tk.Scrollbar(frm_features)
        feature_listbox = tk.Listbox(frm_features, selectmode="multiple", bd=2)
        feature_listbox.grid(row=0, column=0, sticky="ns")
        # yscrollbar.config(command=feature_listbox.yview)
        frm_features.grid(row=0, column=1, sticky="ns")
        return {'feature_listbox': feature_listbox}
